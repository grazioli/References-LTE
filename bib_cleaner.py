#!/usr/bin/python3
'''
Script for checking the similarity between the titles in a "bib" file.
The script DOES NOT MODIFY the original bib file. If you want to eliminate
the titles that are  really the same, you have to delete them MANUALLY.


=====================
	HOW TO LAUNCH
=====================
DEFAULT USAGE
Some default values have been defined:
- the default file to be checked is "ref_biblio.bib",
- the default similarity threshold is 0.75.

If you are happy with these values, just launch the script with:

python3 bib_cleaner.py

And it will print at screen the list of similar titles.


CHANGHING THE DEFAULT PARAMETERS
If you want to change the parameters, you can modify the values of the two
constants at the top of the script:
- DEFAULT_FNAME
- DEFAULT_THRESHOLD

And then  launch the script normally.


LAUNCHING THE SCRIPT ON A DIFFERENT BIB FILE
The script can be launched on a different bib file without the need to modîfy
the default parameters.
For doing so, just give the filename as the first argument of the script
when launching it from the terminal.
As example:

python3 bib_cleaner.py custom_bib_file.bib

(where "custom_bib_file.bib" is just a made up name for a different bib file).


=====================
		OUTPUT
=====================
The script prints at screen a list of all the title that are above the
similarity threshold.
An example of output is:

	Pair of titles with similarity greater than 0.75:

	Multispectral and hyperspectral remote sensing for identification and mapping of wetland vegetation: a review
	Multispectral and hyperspectral remote sensing for identification and mapping of wetland vegetation


	Spatial and temporal analysis of dry spells in {G}reece
	Spatial and temporal scales in rainfall analysis, some aspects and future perspectives

	[...]

Then the list can be used for checking manually which of the detected similar
titles are really referring to the same article.


=================
Last modified on:
04.05.2018
=================
'''
import os
import sys
from difflib import SequenceMatcher


# ------------------------------------------------------------------------------
# DEFAULT PARAMETERS
DEFAULT_FNAME = "ref_biblio.bib"
DEFAULT_THRESHOLD = 0.75
# ------------------------------------------------------------------------------


def parse_input():
	'''
	Function for reading the user input.
	The script can expect an argument: the filename of the bib file to check.

	Output:
	- fname, a string with the name of the bib file. If none is specified,
				returns DEFAULT_FNAME.
	'''

	# Firs we check if there's an input
	if len(sys.argv) < 2:
		return DEFAULT_FNAME

	fname = sys.argv[1]

	# If there is, we check if it exists
	if not os.path.exists(fname):
		print('\nWARNING! The file specified was not found.')
		print('File specified: ' + fname)
		print('Trying with default filename (' + DEFAULT_FNAME + ')\n')
		return DEFAULT_FNAME
	return fname


def similar(a, b):
	# Just a wrapper on "SequenceMatcher", that returns a single float
	# as a measure of the similarities between two strings
	return SequenceMatcher(None, a, b).ratio()


def main():
	# Parsing user input:
	fname = parse_input()

	f = open(fname, 'r')

	# Starting to read all the content of the title field in the bib file.
	title_list = []
	for line in f:
		# We exclude the field BOOKTITLE
		if 'TITLE' in line.upper() and 'BOOKTITLE' not in line.upper() and 'SHORTTITLE' not in line.upper():
			# In the next line, all the "strip" are just to clean a little bit
			# the title at the beginning and end
			title_list.append(' '.join(line.split()[2:]).strip('{').strip(' ').strip(',').strip('}'))

	# Now we loop an all pair of files, checking the similarity
	print('Pair of titles with similarity greater than ' + str(DEFAULT_THRESHOLD) + ':\n')
	for i in range(len(title_list) - 1):
		# The title we're currently checking
		curr_title = title_list[i]
		for j in range(i + 1, len(title_list)):
			# The title we're checking against
			title_checked_against = title_list[j]
			if similar(curr_title, title_checked_against) > DEFAULT_THRESHOLD:
				print(curr_title)
				print(title_checked_against)
				print('\n')
	return 0


if __name__ == '__main__':
	main()
